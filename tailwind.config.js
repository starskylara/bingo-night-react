module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    screens: {
      "3sm": "320px",
      "2sm": { 'min': '415px', 'max': '568px' },
      sm: "640px",
      "3md": "731px",
      "2md": "768px",
      md: "769px",
      "2lg": "812px",
      lg: "1024px",
      xl: "1280px",
      "2xl": "1536px",
    },
    colors: {
      borderPurple: "#c061a4",
      borderYellow: "#F5EF8C",
      white: "#FFFFFF",
      black: "#000000",
      darkBlue: "#101531",
    },
    textColor: {
      primary: "#F5EF8C",
      secondary: "#8CD4E0",
      success: "#34D399",
      white: "#FFFFFF",
    },
    extend: {
      width: {
        "1/7": "14.2857143%",
        "2/7": "28.5714286%",
        "3/7": "42.8571429%",
        "4/7": "57.1428571%",
        "5/7": "71.4285714%",
        "6/7": "85.7142857%",
      },
      height: {
        "1/7": "14.2857143%",
        "2/7": "28.5714286%",
        "3/7": "42.8571429%",
        "4/7": "57.1428571%",
        "5/7": "71.4285714%",
        "6/7": "85.7142857%",
      },
    },
    fontSize: {
      'xs': '.6rem',
      'sm': '.875rem',
      'tiny': '.875rem',
      'base': '1rem',
      'lg': '1.125rem',
      'xl': '1.25rem',
      '2xl': '1.5rem',
      '3xl': '1.875rem',
      '4xl': '2.25rem',
      '5xl': '3rem',
      '6xl': '4rem',
      '7xl': '5rem',
    }
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
