import Board from "./components/board";
import "./index.css";

function App() {
  return (
    <div className="App bg-darkBlue">
      <Board />
    </div>
  );
}

export default App;
