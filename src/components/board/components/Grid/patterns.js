const getHorizontals = (arr, factor) => {
  if (arr.length <= factor) return [arr];
  return (
    arr.length &&
    [arr.slice(0, factor)].concat(
      getHorizontals(arr.slice(factor, arr.length), factor)
    )
  );
};

const getVerticals = (arr, counter = 0) => {
  if (counter === arr.length - 1) return [arr.map((i) => i[counter])];
  return [arr.map((i) => i[counter])].concat(getVerticals(arr, counter + 1));
};

const getDiagonals = (arr, counter = 0) => {
  if (counter === arr.length - 1) return [arr[counter][counter]];
  return [arr[counter][counter]].concat(getDiagonals(arr, counter + 1));
};

const reverseArray = (arr) => {
  if (arr.length === 1) return arr;
  return [arr[arr.length - 1]].concat(
    reverseArray(arr.slice(0, arr.length - 1))
  );
};

export const getPatterns = (input) => {
  const grid = Array.from(Array(input.length * input.length).keys());
  const horizontals = getHorizontals(grid, input.length);
  const verticals = getVerticals(horizontals);
  const diagonals = [
    getDiagonals(horizontals),
    reverseArray(getDiagonals(reverseArray(horizontals))),
  ];
  return [...horizontals, ...verticals, ...diagonals].map((i) =>
    i.filter((j) => j !== parseInt(grid.length / 2))
  );
};

export const isValidSubsequence = (array, sequence) => {
  let seqCounter = 0;
  for (let i = 0; i < array.length; i++) {
    if (seqCounter === sequence.length) break;
    if (array[i] === sequence[seqCounter]) seqCounter++;
  }
  return seqCounter === sequence.length;
};
