import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import Confetti from "react-confetti";
import { useWindowSize } from "react-use";
import FavoriteIcon from "@material-ui/icons/Favorite";
import LocalBarIcon from "@material-ui/icons/LocalBar";
import Card from "../Card";
import { Wrapper, Text } from "./styled";
import Modal from "../../../modal";
import { getPatterns, isValidSubsequence } from "./patterns";

const STYLE_ICON = {
  color: "#C161A5",
  margin: "auto",
  display: "felx",
  height: "100%",
  width: "100%",
};

const Grid = ({ questions, grid }) => {
  const { width, height } = useWindowSize();
  const [questionary, setQuestionary] = useState(null);
  const [boardChecks, setBoardChecks] = useState(null);
  const [showModal, setShowModal] = useState(false);
  const [currentCartIndex, setCurrentCartIndex] = useState(null);
  const [won, setWon] = useState(false);

  useEffect(() => {
    setQuestionary(questions);
    setBoardChecks(getPatterns(grid));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [grid, questions]);

  useEffect(() => {
    if (questionary && boardChecks) {
      const active = questionary.filter((i) => i.active).map((i) => i.id);
      boardChecks.forEach((i) => {
        if (isValidSubsequence(active, i)) {
          setWon(true);
          setShowModal(true);
        }
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [questionary]);

  const handleClick = (index) => {
    if (questionary[index].active) {
      setShowModal(true);
      setCurrentCartIndex(Number(index));
    } else {
      handleAccept(index);
    }
  };

  const handleAccept = (index) => {
    let idx;
    if (currentCartIndex || currentCartIndex === 0) {
      idx = currentCartIndex;
    }
    if (!isNaN(index)) {
      idx = index;
    }
    if (!isNaN(idx)) {
      const tempQuestionary = [...questionary];
      tempQuestionary[idx] = {
        ...tempQuestionary[idx],
        active: !tempQuestionary[idx].active,
      };
      setQuestionary(tempQuestionary);
      setCurrentCartIndex(null);
      setShowModal(false);
    }
  };

  const handleClosed = () => {
    setCurrentCartIndex(null);
    setShowModal(false);
  };

  const handleWin = () => {
    setWon(false);
    setShowModal(false);
    setQuestionary(questions);
  };

  return (
    <Wrapper
      id="wrapper-grid"
      className="flex h-full w-full flex-col justify-center "
    >
      {won && <Confetti width={width} height={height} />}
      {showModal && (
        <Modal
          onClose={won ? null : handleClosed}
          onAccept={won ? handleWin : handleAccept}
          title={`${won ? "You WON!" : "Do you want to un-mark this question"}`}
          message={
            won ? (
              <p className="text-sm text-white">
                Click on
                <span className="font-bold text-success">{` Accept `}</span> to
                start again
              </p>
            ) : (
              <p className="text-sm text-white">
                Click on the
                <span className="font-bold text-success">{` Accept `}</span>
                button if you want to un-mark the question
                <span className="font-bold text-primary">{` "${
                  currentCartIndex ? questionary[currentCartIndex].text : ""
                }" `}</span>
              </p>
            )
          }
          icon={
            won ? (
              <FavoriteIcon style={STYLE_ICON} />
            ) : (
              <LocalBarIcon style={STYLE_ICON} />
            )
          }
        />
      )}
      {questionary &&
        grid.map((idxR) => (
          <div id="row-grid" className={`flex h-1/${grid.length}`} key={idxR}>
            {grid.map((idxC) =>
              idxR === parseInt(grid.length / 2) &&
              idxC === parseInt(grid.length / 2) ? (
                <div
                  className={`w-1/${grid.length} h-full border-borderPurple p-2  border-t-2 border-r-2 flex justify-center content-center flex-wrap`}
                  key={idxC}
                >
                  <div className="w-full h-full p-2 border border-borderYellow rounded-2xl flex flex-col">
                    <div className="flex xl:h-14 lg:h-14 sm:h-full m-auto">
                      <FavoriteIcon style={STYLE_ICON} />
                    </div>
                    {/* The idea is to use style components but i like tail wind */}
                    <Text className="lg:text-lg md:text-xs 3sm:hidden 2lg:hidden sm:hidden 3md:hidden md:block lg:block">
                      Free
                    </Text>
                  </div>
                </div>
              ) : (
                <div
                  className={`w-1/${
                    grid.length
                  } h-full border-borderPurple border-t-2 border-r-2 rounded-2xl" ${
                    idxC === 0 ? "border-l-2" : ""
                  } ${idxR === grid.length - 1 ? "border-b-2" : ""}`}
                  key={idxC}
                  onClick={(e) => handleClick(idxR * grid.length + idxC)}
                >
                  <Card
                    text={
                      questionary[idxR * grid.length + idxC] &&
                      questionary[idxR * grid.length + idxC].text
                    }
                    active={
                      questionary[idxR * grid.length + idxC] &&
                      questionary[idxR * grid.length + idxC].active
                    }
                  />
                </div>
              )
            )}
          </div>
        ))}
    </Wrapper>
  );
};

Grid.propTypes = {
  questions: PropTypes.array,
  grid: PropTypes.array,
};
Grid.defaultProps = {
  questions: [],
  grid: [0, 1, 2, 3, 4],
};

export default Grid;
