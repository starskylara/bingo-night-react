import React from "react";
import PropTypes from "prop-types";
import "./styled.css";

const Card = ({ text, active }) => {
  return (
    <div className={`flip-card cursor-pointer ${active ? "p-1 flip" : ""}`}>
      <div className="flip-card-inner">
        <div className="overflow-ellipsis overflow-hidden flip-card-front">
          <div className="flip-card-text h-full w-full p-1">
            <p className="content-front text-white lg:text-lg md:text-md sm:text-xs 3sm:text-xs p-auto md:p-3 sm:p-2">
              {text}
            </p>
          </div>
        </div>
        <div className="overflow-ellipsis overflow-hidden flip-card-back border-2 border-borderYellow rounded-md">
          <div className="flip-card-text h-full w-full p-1">
            <p className="content-front text-white lg:text-lg md:text-md sm:text-xs 3sm:text-xs p-auto">
              {text}
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

Card.propTypes = {
  text: PropTypes.string,
  active: PropTypes.bool,
  className: PropTypes.string,
};
Card.defaultProps = {
  text: "Empty",
  active: false,
};

export default Card;
