import React, { useEffect, useState } from "react";
// import PropTypes from "prop-types";
import Grid from "./components/Grid";
import Questions from "./questions.json";
import Instructions from "./instructions.json";
import HeaderBoard from "../../assets/images/board-header.png";

const GRID = [
  { id: 0, label: "Grid size 5 x 5", value: [0, 1, 2, 3, 4] },
  { id: 1, label: "Grid size 3 x 3", value: [0, 1, 2] },
];

const Board = () => {
  const [board, setBoard] = useState(null);
  const [currentGrid, setGrid] = useState(GRID[0]);

  useEffect(() => {
    getBoard(currentGrid);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [currentGrid]);

  const random = (min, max) => Math.floor(Math.random() * (max - min)) + min;

  const shuffle = (grid, counter) => {
    if (counter === 0) return random(0, grid);
    return [random(0, grid)].concat(shuffle(grid, counter - 1));
  };

  const getBoard = (input) => {
    const shuffleQuestions = shuffle(
      Questions.length,
      input.value.length * input.value.length - 1
    );
    const temp = shuffleQuestions.map((i, index) => ({
      id: index,
      text: Questions[i],
      active: false,
    }));
    setBoard(temp);
  };

  const handleSelectNewGrid = (e) => {
    const temp = GRID.find((i) => i.id === Number(e.target.value));
    setGrid(temp);
  };

  return (
    <div className="flex bg-darkBlue lg:flex-row md:flex-row 2md:flex-col 3md:flex-row sm:flex-row 2sm:flex-row 3sm:flex-col h-screen">
      <div className="lg:pt-10 lg:pl-10 lg:pb-10 sm:pt-5 sm:pl-5 sm:pr-5 sm:pb-5 2sm:pt-5 2sm:pl- 2sm:pr-5 3sm:pt-5 3sm:pl-5 3sm:pr-5 3sm:pb-0 2xl:h-full lg:w-3/5 lg:h-full sm:h-full sm:w-3/5 2sm:w-3/5 3md:h-full 3md:w-3/5 flex items-start lg:flex-col sm:flex-col 3md:flex-col 3sm:flex-col 2md:m-auto">
        <div
          id="header-grid"
          alt="bingo logo"
          style={{ backgroundImage: `url(${HeaderBoard})` }}
          className="lg:h-3/5 lg:w-full sm:h-full sm:w-full 3sm:w-full 3sm:h-60 bg-center bg-contain bg-no-repeat"
        />
        <div className="w-full lg:h-full md:w-full sm:w-full sm:h-full sm:pl-0 sm:pr-0 2sm:h-full 3sm:h-px overflow-y-auto lg:pl-0 lg:pr-0">
          <div className="w-full pl-7 pt-5 pr-7">
            <select
              className="w-full bg-darkBlue p-1 text-primary border-2 rounded-md border-borderPurple"
              onChange={handleSelectNewGrid}
            >
              {GRID.map((i, index) => (
                <option key={index} value={i.id} defaultValue={currentGrid}>
                  {i.label}
                </option>
              ))}
            </select>
          </div>
          <p className="pl-7 pt-5 pb-3 2sm:pl-2 2sm:pr-1 2sm:pb-1 capitalize text-primary 3sm:text-sm">
            instructions
          </p>
          <ul className="pl-7 pr-2 2sm:pl-2 2sm:pr-1 list-decimal 3sm:text-sm ">
            {Instructions.map((i, index) => (
              <li
                className="pb-5 text-sm text-secondary 3sm:text-sm"
                key={index}
              >
                {i}
              </li>
            ))}
          </ul>
        </div>
      </div>
      <div className="p-5 h-full w-full">
        {board && <Grid questions={board} grid={currentGrid.value} />}
      </div>
    </div>
  );
};

Board.propTypes = {};

export default Board;
